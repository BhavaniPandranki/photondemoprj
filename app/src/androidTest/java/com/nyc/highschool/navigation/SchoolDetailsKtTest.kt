package com.nyc.highschool.navigation

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.nyc.highschool.models.School
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SchoolDetailsKtTest{
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun checkSchoolDetails_With_CorrectArg() {
        val schoolInfo = School("21K728", "Liberation Diploma Plus High School", "The mission of Liberation Diploma Plus High School, in partnership with CAMBA, is to develop the student academically, socially, and emotionally.")

        // Start the app
        composeTestRule.setContent {
            ShowDetails(schoolInfo.dbn,schoolInfo.overview_paragraph,{false})
        }

        composeTestRule.onNodeWithText(schoolInfo.dbn).assertIsDisplayed()
        composeTestRule.onNodeWithContentDescription("Back").performClick()

    }
}