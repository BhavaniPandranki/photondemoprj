package com.nyc.highschool

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.*
import com.nyc.highschool.datalayer.ApiState
import com.nyc.highschool.datalayer.SchoolApiService
import kotlinx.coroutines.launch

//MVVM architecture is implemented.
//following MainViewModel holds ViewModel for first screen where schools are displayed
//state / livedata / flow variable can be used to holds api response
class MainViewModel(val schoolApiService: SchoolApiService) : ViewModel() {

    //MutableState is used as the whole list of json items(School object) are loaded at once
    // and compose to recompose on updating state.
    //Alternatively Can use lifecycle aware- flow if we iterate through list and emit
    val schoolResponseState: MutableState<ApiState> = mutableStateOf(ApiState.Empty)

    init {
        //Fetches schools on instantiating MainViewModel in activity
        getSchools()
    }

    private fun getSchools() {
        //Reason to use viewModelScope is to cancel this coroutine when all dependencies of this viewModel is freed
        viewModelScope.launch {
            //getSchools is suspended function and ran in viewmodel scope to terminate as soon as viewmodel destroyed.
            //latency in network calls doesn't block UI
            schoolResponseState.value = ApiState.Loading
            try {
                val response = schoolApiService.getSchools()
                if (response.isSuccessful()) {
                    schoolResponseState.value =
                        if (response?.body().isNullOrEmpty()) ApiState.Empty else ApiState.Success(
                            response?.body() ?: emptyList()
                        )
                } else {
                    schoolResponseState.value = ApiState.Failure(Exception(response.message()))
                }
            } catch (e: Exception) {
                // Handle errors here
                schoolResponseState.value = ApiState.Failure(e)
            }
        }
    }
}