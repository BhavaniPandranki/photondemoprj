package com.nyc.highschool

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.nyc.highschool.models.School

fun readJsonFromAssets(context: Context, fileName: String): String {
    return context.assets.open(fileName).bufferedReader().use { it.readText() }
}

fun parseJsonToModel(jsonString: String): List<School> {
    val gson = Gson()
    return gson.fromJson(jsonString, object : TypeToken<List<School>>() {}.type)
}

fun getSelectedSchool(dbn: String, schools: List<School>) : School{
    return schools.find { it.dbn == dbn } ?: School("","","")
}