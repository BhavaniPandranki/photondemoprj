package com.nyc.highschool.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.*
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.nyc.highschool.MainViewModel
import com.nyc.highschool.datalayer.ApiState
import com.nyc.highschool.getSelectedSchool

@Composable
fun AppNavigation(viewModel: MainViewModel) {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = Screen.MainScreen.route){
            composable(route = Screen.MainScreen.route){
                val schoolState = viewModel.schoolResponseState.value
                if(schoolState!=null) ListSchools(schoolState,onSelect={selSchool ->
                    navController.navigate(Screen.DeatailScreen.route+"/$selSchool")})
            }
            composable(route = Screen.DeatailScreen.route+"/{dbn}",
                arguments = listOf(
                    navArgument("dbn"){
                        type= NavType.StringType
                        nullable=false
                    }
                )
            ){entry ->
                //selected item(dbn key) is passed to detailed screen via path argument
                //note: can use shared viewModel to pass this info.
                val selSchool = entry.arguments?.getString("dbn") ?: ""
                val schoolState = viewModel.schoolResponseState.value
                if(schoolState is ApiState.Success){
                    //finds corresponding overview of school & pass to details compose fun
                    val selectedSchool = getSelectedSchool(selSchool, schoolState.data)
                    ShowDetails(dbn = selectedSchool.dbn, overview = selectedSchool.overview_paragraph, onBackNav={
                        //goes to previous screen i.e. schools list screen
                        navController.popBackStack()
                    } )
                }
            }
    }
}