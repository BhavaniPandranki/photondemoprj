package com.nyc.highschool.navigation

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.nyc.highschool.R
import com.nyc.highschool.datalayer.ApiState
import com.nyc.highschool.models.School


//Displays school name & dbn. Represents item in the list
@Composable
fun ShowTitle(name: String, dbn: String, onSelect:(String)-> Unit) {
    Column(
        verticalArrangement = Arrangement.SpaceEvenly,
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)
            .clickable(onClick = {
                onSelect(dbn) })
    ) {
        Text(
            text = dbn,
            color = Color.Blue,
        )
        Text(
            text = name,
            color = Color.Green,
            fontSize = 18.sp,
            modifier = Modifier
                .padding(top = 8.dp)
        )

        Divider()
    }

}

@Composable
fun RenderSchools(schools: List<School>, onSelect: (String)-> Unit) {
    Column(verticalArrangement = Arrangement.Top) {
        Text(
            text = "Schools in NewYork:",
            color = Color.Red,
            fontSize = 22.sp,
            modifier = Modifier.padding(10.dp)
        )
        LazyColumn {
            items(schools) {
                ShowTitle(
                    name = it.school_name.toString(),
                    dbn = it.dbn.toString(),
                    onSelect
                )
            }
        }
    }
}

/*
* Based on Api response, either shows list of schools or corresponding API status is shown
* */
@Composable
fun ListSchools(schoolState: ApiState, onSelect: (String)->Unit) {
//    //temporarily using assets to display, until Retrofit implements
//    val schoolsInfoInJson = readJsonFromAssets(context, "data.json")
//    val schools = parseJsonToModel(schoolsInfoInJson)

    //On change in schoolState with API response, dashboard content updated accordingly
    when (schoolState) {
        is ApiState.Success -> {
            //Renders list of schools on API success
            RenderSchools(schoolState.data, onSelect)
        }
        is ApiState.Empty -> {
            Text(
                text = stringResource(id = R.string.empty),
                color = Color.Red,
                fontSize = 22.sp,
                modifier = Modifier.padding(10.dp)
            )
        }
        is ApiState.Failure -> {
            Text(
                text = stringResource(id = R.string.api_error),
                color = Color.Red,
                fontSize = 22.sp,
                modifier = Modifier.padding(10.dp)
            )
        }
        is ApiState.Loading -> {
            Text(
                text = stringResource(id = R.string.loading),
                color = Color.Red,
                fontSize = 22.sp,
                modifier = Modifier.padding(10.dp)
            )
        }
    }

}
