package com.nyc.highschool.navigation

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.nyc.highschool.models.School

//Displays school details like name & dbn
//Content to display and navigation callback are passed as arguments
//Thus, State hoisting pattern is implemented to make this compose as stateless
@Composable
fun ShowDetails(dbn:String, overview: String, onBackNav:()-> Boolean) {
    //header is shown with back button to navigate to ListScreen.
    Scaffold(
        topBar = {
            Row (verticalAlignment = Alignment.CenterVertically){
                Icon(
                    imageVector = Icons.Filled.ArrowBack,
                    contentDescription = "Back",
                    modifier = Modifier
                        .padding(16.dp)
                        .clickable {
                            onBackNav()
                        }
                )
                Text(text = dbn,
                color=Color.Magenta)
            }
        }

    ) {innerPadding ->
        Column(
            modifier = Modifier
                .padding(innerPadding),
            verticalArrangement = Arrangement.spacedBy(16.dp),
        ) {
            Text(
                text = overview,
                color = Color.Blue,
                modifier = Modifier.padding(8.dp)
            )
        }
    }
}