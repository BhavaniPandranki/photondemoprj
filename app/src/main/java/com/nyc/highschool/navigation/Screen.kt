package com.nyc.highschool.navigation

sealed class Screen(val route: String){
    object MainScreen: Screen("main_screen")
    object DeatailScreen: Screen("detail_screen")
}
