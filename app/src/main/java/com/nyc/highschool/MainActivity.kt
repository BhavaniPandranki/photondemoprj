package com.nyc.highschool

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.nyc.highschool.datalayer.ApiServer
import com.nyc.highschool.navigation.AppNavigation
import com.nyc.highschool.ui.theme.NycHighSchoolTheme

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val viewModel = MainViewModel(ApiServer.getSchoolService(ApiServer.getRetrofitInstance()))
        setContent {
            NycHighSchoolTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(), color = MaterialTheme.colors.background
                ) {
                    //Navigation compose implemented to traverse between list screen & detailed screen
                    AppNavigation(viewModel)
                }
            }
        }
    }
}


//To preview UI while developing
@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    NycHighSchoolTheme {
        AppNavigation(MainViewModel(ApiServer.getSchoolService(ApiServer.getRetrofitInstance())))
    }
}