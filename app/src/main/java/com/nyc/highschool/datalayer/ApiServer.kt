package com.nyc.highschool.datalayer

import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

//further can enhance dependency management with Dagger2.0 or Hilt(an extension of Dagger2.0 only applicable for Android framework)
class ApiServer {
    companion object{
        val BASE_URL = "https://data.cityofnewyork.us/"

        fun getRetrofitInstance(): Retrofit {
            return Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create())).build()
        }

        //For each micro service or module ApiService created for readability
        fun getSchoolService(retrofitInst: Retrofit): SchoolApiService {
            return retrofitInst.create(SchoolApiService::class.java)
        }

        //note: create other services for each module or micro service
    }

}