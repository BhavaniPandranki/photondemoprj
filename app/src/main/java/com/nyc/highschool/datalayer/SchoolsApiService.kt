package com.nyc.highschool.datalayer

import com.nyc.highschool.models.School
import retrofit2.Response
import retrofit2.http.GET

//Defines all School API
interface SchoolApiService {
    //end point defined to fetch json file
    @GET("/resource/s3k6-pzi2.json")
    suspend fun getSchools(): Response<List<School>>
}