package com.nyc.highschool.datalayer

import com.nyc.highschool.models.School

sealed class ApiState {
    class Success(val data: List<School>) : ApiState()
    class Failure(val msg: Throwable) : ApiState()
    object Loading:ApiState()
    object Empty: ApiState()
}