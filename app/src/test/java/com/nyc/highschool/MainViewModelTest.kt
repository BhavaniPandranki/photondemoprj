package com.nyc.highschool

import com.google.common.truth.Truth.assertThat
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.nyc.highschool.datalayer.ApiServer
import com.nyc.highschool.datalayer.ApiState
import com.nyc.highschool.datalayer.SchoolApiService
import com.nyc.highschool.models.School
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.*
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer

import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.HttpURLConnection

@ExperimentalCoroutinesApi
class MainViewModelTest {
    private  lateinit var viewModel: MainViewModel;
    private  lateinit var schoolApiServer: SchoolApiService;
    private  lateinit var server: MockWebServer;
    val dispatcher = UnconfinedTestDispatcher()

    @Before
    fun setUp() {
        //creates mock server
        server = MockWebServer()

        //used to serialize api response
        val gson : Gson = GsonBuilder()
            .setLenient()
            .create();

        //Build retrofit above mock server
        val mockRetrofit : Retrofit = Retrofit.Builder()
            .baseUrl(server.url("/"))//Pass any base url like this
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        schoolApiServer = ApiServer.getSchoolService(mockRetrofit)

        //dispatcher set to main, to run coroutine which is lauched in VM to make api call
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun tearDown() {
        //shutting down server after each test case
        server.shutdown()

        Dispatchers.resetMain()
    }

    @Test
    fun fetchesEmptySchoolFromApi() = runTest{
        val res = MockResponse()//Make a fake response for our server call
        res.setResponseCode(HttpURLConnection.HTTP_OK)
        res.setBody("[]")//set empty mock response
        server.enqueue(res)//add it in the server response queue

        //on instantiating this viewModel, it initiates fetching school api
        viewModel = MainViewModel(schoolApiService = schoolApiServer)

        //Mocking response of fetchSchoolApi
        server.takeRequest()

        //suspends test until, state changes from Loading to some other(Success/Failure)
        do{
            delay(1000)
        }while(viewModel.schoolResponseState.value is ApiState.Loading)

        // will run the child coroutine to completion
        testScheduler.advanceUntilIdle()

        with(viewModel.schoolResponseState){
            assertThat(value).isNotNull()
            assertThat((value as (ApiState.Empty))).isNotNull()
        }
    }

    @Test
    fun fetchesSchoolsFromApi() = runTest{
        val schools = mutableListOf<School>()
        val school1 = School("21K728", "Liberation Diploma Plus High School", "The mission of Liberation Diploma Plus High School, in partnership with CAMBA, is to develop the student academically, socially, and emotionally.")
        val school2 = School(
            "02M260",
            "Clinton School Writers & Artists, M.S. 260",
            "Students who are prepared for college must have an education"
        )
        schools.add(school1)
        schools.add(school2)

        val jsonResponse =  Gson().toJson(schools)

        val res = MockResponse()//Make a fake response for our server call
        res.setResponseCode(HttpURLConnection.HTTP_OK)
        res.setBody(jsonResponse)//set mock response
        server.enqueue(res)//add it in the server response queue

        //on instantiating this viewModel, it initiates fetching school api
        viewModel = MainViewModel(schoolApiService = schoolApiServer)

        //Mocking response of fetchSchoolApi
        server.takeRequest()

        //suspends test until, state changes from Loading to some other(Success/Failure)
        do{
            delay(1000)
        }while(viewModel.schoolResponseState.value is ApiState.Loading)

        // will run the child coroutine to completion
        testScheduler.advanceUntilIdle()

        with(viewModel.schoolResponseState){
            assertThat(value).isNotNull()
            assertThat((value as (ApiState.Success))).isNotNull()
            assertThat((value as (ApiState.Success)).data).isNotEmpty()
        }
    }

    @Test
    fun errorWhileFetchingSchoolsFromApi() = runTest{
        val res = MockResponse()//Make a fake response for our server call
        res.setResponseCode(HttpURLConnection.HTTP_NOT_FOUND)
        res.setBody("[]")//set empty mock response
        server.enqueue(res)//add it in the server response queue

        //on instantiating this viewModel, it initiates fetching school api
        viewModel = MainViewModel(schoolApiService = schoolApiServer)

        //Mocking response of fetchSchoolApi
        server.takeRequest()

        //suspends test until, state changes from Loading to some other(Success/Failure)
        do{
            delay(1000)
        }while(viewModel.schoolResponseState.value is ApiState.Loading)

        // will run the child coroutine to completion
        testScheduler.advanceUntilIdle()

        with(viewModel.schoolResponseState){
            assertThat(value).isNotNull()
            assertThat((value as (ApiState.Failure))).isNotNull()
        }
    }

}