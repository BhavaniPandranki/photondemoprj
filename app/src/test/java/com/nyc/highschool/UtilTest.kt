package com.nyc.highschool

import androidx.compose.ui.platform.LocalContext
import com.google.common.truth.Truth.assertThat
import com.nyc.highschool.models.School
import org.junit.Before
import org.junit.Test

class UtilTest{

    @Test
    fun `Searching for a school in empty School list`(){
        val selSchool = getSelectedSchool("02M260", emptyList())

        //On not fouding school, returns empty object
        assertThat(selSchool).isNotNull()
        assertThat(selSchool.dbn).isEmpty()
        assertThat(selSchool.overview_paragraph).isEmpty()
        assertThat(selSchool.school_name).isEmpty()
    }

    @Test
    fun `finding school from list of Schools`(){
        //test inputs: list of School
        val schools = mutableListOf<School>()
        val school1 = School("21K728", "Liberation Diploma Plus High School", "The mission of Liberation Diploma Plus High School, in partnership with CAMBA, is to develop the student academically, socially, and emotionally.")
        val school2 = School(
            "02M260",
            "Clinton School Writers & Artists, M.S. 260",
            "Students who are prepared for college must have an education"
        )
        schools.add(school1)
        schools.add(school2)

        //searching for school1
        val selSchool = getSelectedSchool("21K728", schools)

        assertThat(selSchool).isNotNull()
        assertThat(selSchool.dbn).isEqualTo(school1.dbn)
        assertThat(selSchool.overview_paragraph).isEqualTo(school1.overview_paragraph)
        assertThat(selSchool.school_name).isEqualTo(school1.school_name)
    }

    @Test
    fun `Searching school which is not in list`(){
        //test inputs: list of School
        val schools = mutableListOf<School>()
        val school1 = School("21K728", "Liberation Diploma Plus High School", "The mission of Liberation Diploma Plus High School, in partnership with CAMBA, is to develop the student academically, socially, and emotionally.")
        val school2 = School(
            "02M260",
            "Clinton School Writers & Artists, M.S. 260",
            "Students who are prepared for college must have an education"
        )
        schools.add(school1)
        schools.add(school2)

        //searching for school1
        val selSchool = getSelectedSchool("21K729", schools)

        assertThat(selSchool).isNotNull()
        assertThat(selSchool.dbn).isEmpty()
        assertThat(selSchool.overview_paragraph).isEmpty()
        assertThat(selSchool.school_name).isEmpty()
    }

    @Test
    fun `Searching empty school details in list`(){
        //test inputs: list of School
        val schools = mutableListOf<School>()
        val school1 = School("21K728", "Liberation Diploma Plus High School", "The mission of Liberation Diploma Plus High School, in partnership with CAMBA, is to develop the student academically, socially, and emotionally.")
        val school2 = School(
            "02M260",
            "Clinton School Writers & Artists, M.S. 260",
            "Students who are prepared for college must have an education"
        )
        schools.add(school1)
        schools.add(school2)

        //searching for school1
        val selSchool = getSelectedSchool("", schools)

        assertThat(selSchool).isNotNull()
        assertThat(selSchool.dbn).isEmpty()
        assertThat(selSchool.overview_paragraph).isEmpty()
        assertThat(selSchool.school_name).isEmpty()
    }
}